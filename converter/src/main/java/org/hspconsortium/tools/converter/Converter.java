package org.hspconsortium.tools.converter;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.parser.IParser;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hl7.fhir.convertors.NullVersionConverterAdvisor;
import org.hl7.fhir.convertors.VersionConvertorAdvisor;
import org.hl7.fhir.convertors.VersionConvertor_10_30;
import org.hl7.fhir.exceptions.FHIRException;
import org.hl7.fhir.instance.model.Bundle;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

/**
 * Hello world!
 */
public class Converter {

    public static String INPUT_DIR = "smart-dstu2";
    public static String OUTPUT_DIR = "out";

    public static void main(String[] args) throws IOException, FHIRException {

        Log log = LogFactory.getLog(Converter.class);

        Path outputDirPath = Paths.get(OUTPUT_DIR);
        if(!Files.exists(outputDirPath)){
            Files.createDirectory(outputDirPath);
        }

        PathMatchingResourcePatternResolver resourceResolver = new PathMatchingResourcePatternResolver();
        org.springframework.core.io.Resource[] resourceFiles = resourceResolver.getResources(String.format("classpath:%s/*", INPUT_DIR));

        int totalFiles = resourceFiles.length;
        log.info(String.format("Found %s files to be converted.", totalFiles));

        // converting from DSTU2, FHIR 1.0.2
        FhirContext fhirContextDstu2 = FhirContext.forDstu2Hl7Org();
        IParser xmlParserDstu2 = fhirContextDstu2.newXmlParser();

        // converting to STU3, FHIR v1.9.0
        FhirContext fhirContextDstu3 = FhirContext.forDstu3();
        IParser jsonParserDstu3 = fhirContextDstu3.newJsonParser();

        // Create converter
        VersionConvertorAdvisor advisor = new NullVersionConverterAdvisor();
        VersionConvertor_10_30 converter = new VersionConvertor_10_30(advisor);

        int count = 0;
        for (Resource currentFile : resourceFiles) {
            log.info(String.format("Converting file %s of %s", ++count, totalFiles));

            BufferedReader bufferedReader = Files.newBufferedReader(currentFile.getFile().toPath());
            // right now we're assuming that we are just converting bundles
            Bundle bundle = xmlParserDstu2.parseResource(Bundle.class, bufferedReader);
            bufferedReader.close();

            try {
                org.hl7.fhir.dstu3.model.Bundle bundleStu3 = converter.convertBundle(bundle);

                String newFileName = currentFile.getFilename().substring(0, currentFile.getFilename().lastIndexOf("."));
                newFileName += ".json";

                Path outputPath = Paths.get(OUTPUT_DIR, newFileName);

                try (BufferedWriter writer = Files.newBufferedWriter(outputPath, StandardOpenOption.CREATE)) {
                    jsonParserDstu3.encodeResourceToWriter(bundleStu3, writer);
                }
            } catch (Error err){
                log.error(String.format("Error converting file %s.", count), err);
            }
        }
    }
}